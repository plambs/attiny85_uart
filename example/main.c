#include "uart.h"

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

int main (void)
{
	uint8_t cpt = 0;

	/* Enable the Prescaler change */
	CLKPR = (1 << CLKPCE);
	/* Reset the Prescaler to go at 8Mhz */
	CLKPR = 0;

	/* Init a software uart on pin PB1 (USI output) */
	UART_init(115200);

	while(1)
	{
		printf("Hello World! %d\n\r", cpt++);

		_delay_ms(1000);
	}

	return 0;
}
