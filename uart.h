#ifndef UART_H
#define UART_H

#include <stdint.h>

void UART_init(uint32_t baudrate);
void UART_tx_str(char* string);

/* You can also use printf directly */

#endif
