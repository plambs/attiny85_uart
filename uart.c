/* Software ("bit-bang") UART Transmitter (8 data bits, 1 stop bit, no parity)
 * for Attiny24A/44A/84A using the internal 8MHz oscillator as clock source
 *
 *
 * (c) 2018 Marcel Meyer-Garcia
 *
 * The original piece of this code is comming from Meyer-Garcia and can be found at
 * the following link, it was published under MIT licence.
 * https://github.com/MarcelMG/AVR8_BitBang_UART_TX/blob/master/main.c
 *
 * The last modification were done by Pierre-Antoine Lambs.
 */

/* NOTE: since the internal 8MHz oscillator is not very accurate, the value for OCR0A can be tuned
 * to achieve the desired baud rate (nominal value is 103)
 */

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

volatile uint16_t tx_shift_reg = 0;
volatile uint8_t timer_prescalar = 0;

/* To be able to use printf */
static int uart_putchar(char c, FILE *stream);
static FILE uart_stdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

static void UART_tx(char character)
{
	uint16_t local_tx_shift_reg = tx_shift_reg;

	//if sending the previous character is not yet finished, return
	//transmission is finished when tx_shift_reg == 0
	if(local_tx_shift_reg){return;}

	//fill the TX shift register witch the character to be sent and the start & stop bits (start bit (1<<0) is already 0)
	local_tx_shift_reg = (character<<1) | (1<<9); //stop bit (1<<9)
	tx_shift_reg = local_tx_shift_reg;

	//start timer0
	TCCR0B |= timer_prescalar;
}

static int uart_putchar(char c, FILE *stream)
{
	if (c == '\n'){
		uart_putchar('\r', stream);
	}

	UART_tx(c);

	/* Wait until tx is done */
	while(tx_shift_reg);

	return 0;
} 

void UART_tx_str(char* string){
	while( *string ){
		UART_tx( *string++ );
		//wait until transmission is finished
		while(tx_shift_reg);
	}
}

static uint8_t find_prescalar(uint32_t baudrate){
	uint8_t prescalar = 1;

	while(((F_CPU / prescalar) / baudrate) > 255){
		switch(prescalar)
		{
			case 1:
				prescalar = 8;
				break;
			case 8:
			default:
				prescalar = 64;
				break;
		}
	}

	return prescalar;
}

void UART_init(uint32_t baudrate){
	uint8_t computed_prescalar = 0;

	//set TX (PB1) pin as output
	DDRB |= _BV(DDB1);
	PORTB |= _BV(PORTB1);

	//find prescalar and memorise it.
	computed_prescalar = find_prescalar(baudrate);

	switch(computed_prescalar){
		case 1:
			/* No prescalar */
			timer_prescalar = (_BV(CS00));
			break;
		case 8:
			/* Clock/8 */
			timer_prescalar = (_BV(CS00) | _BV(CS01));
			break;
		case 64:
			/* Falltrougth*/
		default:
			/* Clock/64 */
			timer_prescalar = (_BV(CS00) | _BV(CS01));
			break;
	}

	//set timer0 to CTC mode
	TCCR0A = _BV(WGM01); // TODO Check = not |= ???

	//enable output compare 0 A interrupt
	TIMSK |= _BV(OCF0A);

	/* Set the value to achive the desired baudrate */
	OCR0A = ((F_CPU/computed_prescalar)/baudrate);

	//enable interrupts
	sei();

	// Redirect stdout to the uart
	stdout = &uart_stdout;
}

//timer0 compare A match interrupt
ISR(TIM0_COMPA_vect)
{
	uint16_t local_tx_shift_reg = tx_shift_reg;

	//output LSB of the TX shift register at the TX pin
	if( local_tx_shift_reg & 0x01 )
	{
		PORTB |= _BV(PORTB1);
	}
	else
	{
		PORTB &=~ _BV(PORTB1);
	}

	//shift the TX shift register one bit to the right
	local_tx_shift_reg >>= 1;
	tx_shift_reg = local_tx_shift_reg;

	//if the stop bit has been sent, the shift register will be 0
	//and the transmission is completed, so we can stop & reset timer0
	if(!local_tx_shift_reg)
	{
		TCCR0B = 0;
		TCNT0 = 0;
	}
}
